;;
;;
;;
;;

;;;; Code:

(my-dot-emacs-install-package 'kotlin-mode)
(my-dot-emacs-install-package 'flycheck-kotlin)
(my-dot-emacs-install-package 'gradle-mode)
(my-dot-emacs-install-package 'flycheck-gradle)

(require 'kotlin-mode)
(require 'flycheck-kotlin)

(with-eval-after-load 'kotlin-mode
  (defun my-kotlin-mode-defaults ()
    (flycheck-kotlin-setup)
    (flycheck-mode +1)
    (gradle-mode +1)
    (auto-fill-mode +1)
    (setf gradle-use-gradlew t)
    (setf gradle-gradlew-executable "./gradlew")
    (setq kotlin-tab-width 4)
    (subword-mode +1))
  (setq my-kotlin-mode-hook 'my-kotlin-mode-defaults)
  (add-hook 'kotlin-mode-hook (lambda ()
                              (run-hooks 'my-kotlin-mode-hook))))

(provide 'my-kotlin)

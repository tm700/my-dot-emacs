;;; Code:

;; Most basic UI
(setq visible-bell +1)
(setq ring-bell-function 'ignore)

(blink-cursor-mode -1)

(scroll-bar-mode -1) ; no scroll-bar
(tool-bar-mode -1)   ; no tool bar
(menu-bar-mode -1)   ; no menu bar

(setq make-backup-files nil) ; Stop creating backup files
(setq inhibit-startup-screen t) ; disable startup screen

;; mode line settings
(line-number-mode t)
(column-number-mode t)
(size-indication-mode t)

;; enable y/n answers
(fset 'yes-or-no-p 'y-or-n-p)

;; show the cursor when moving after big movements in the window
;; From beacon.el
;; Whenever the window scrolls a light will shine on top of your cursor so
;; you know where it is.
(require 'beacon)
(beacon-mode +1)

;; show available keybindings after you start typing
(require 'which-key)
(which-key-mode +1)

;; VC (version control) hightlighting.
(require 'diff-hl)
(global-diff-hl-mode +1)

(load-theme 'dracula)

(provide 'my-ui)
;;; my-ui.el ends here

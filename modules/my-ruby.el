;;
;;
;;
;;

;;;; Code:

(my-dot-emacs-install-package 'inf-ruby)
(my-dot-emacs-install-package 'yari)
(my-dot-emacs-install-package 'rspec-mode)
(my-dot-emacs-install-package 'web-mode)

(require 'inf-ruby)
(require 'yari)
(require 'rspec-mode)

(add-to-list 'completion-ignored-extensions ".rbc")

(with-eval-after-load 'ruby-mode
  (defun my-ruby-mode-defaults ()
    (flycheck-mode +1)
    (inf-ruby-minor-mode +1)
    ;; CamelCase aware editing operations
    (subword-mode +1))
  (setq my-ruby-mode-hook 'my-ruby-mode-defaults)
  (add-hook 'ruby-mode-hook (lambda ()
                              (run-hooks 'my-ruby-mode-hook))))

(provide 'my-ruby)

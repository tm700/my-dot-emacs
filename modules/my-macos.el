;;; Code:

(my-dot-emacs-install-package 'exec-path-from-shell)

(require 'exec-path-from-shell)
(exec-path-from-shell-initialize)

(provide 'my-macos)
;;; my-macos.el ends here

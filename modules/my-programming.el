;;; Code:

(add-hook 'prog-mode-hook (lambda ()
                            (flyspell-prog-mode)
                            (whitespace-mode +1)
                            (add-hook 'before-save-hook
                                      (lambda () (whitespace-cleanup)) nil t)
                            (set (make-local-variable 'comment-auto-fill-only-comments) t)
                            (smartparens-mode +1)))

;; font-lock annotations like TODO in source code

(my-dot-emacs-install-package 'hl-todo)
(require 'hl-todo)
(global-hl-todo-mode 1)

(provide 'my-programming)

;;; my-programming.el ends here

;;;

;;; Code:

(global-set-key (kbd "C-x \\") 'align-regexp)
(global-set-key (kbd "C-x rp") 'replace-regexp)

(provide 'my-global-keybindings)
;;; my-global-keybindings.el ends here

;;; Code:

(require 'eshell)

(my-dot-emacs-install-package 'eshell-git-prompt)
(eshell-git-prompt-use-theme "powerline")

(provide 'my-eshell)
;;; my-eshell.el ends here

;;; packagage  --- Summary

;;; Commentary:

;;; Code:

;;;; tabs and spaces
(setq-default indent-tabs-mode nil)   ;; don't use tabs to indent
(setq-default tab-width 8)            ;; but maintain correct appearance

;; smart tab behavior - indent or complete
(setq tab-always-indent 'complete)

;; Newline at end of file
(setq require-final-newline t)

;; delete the selection with a keypress
(delete-selection-mode t)

;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; autosave the undo-tree history
(setq undo-tree-history-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq undo-tree-auto-save-history t)

;; revert buffers automatically when underlying files are changed externally
(global-auto-revert-mode t)

;; saveplace remembers your location in a file when saving files
(setq save-place-file (expand-file-name "saveplace" my-dot-emacs-savefile-dir))
;; activate it for all buffers
(save-place-mode 1)

;; savehist keeps track of some history
(require 'savehist)
(setq savehist-additional-variables
      ;; search entries
      '(search-ring regexp-search-ring)
      ;; save every minute
      savehist-autosave-interval 60
      ;; keep the home clean
      savehist-file (expand-file-name "savehist" my-dot-emacs-savefile-dir))
(savehist-mode +1)

;; A minor mode that saves Emacs buffers when they lose focus.
(require 'super-save)
(super-save-mode +1)

;; highlight the current line
(global-hl-line-mode +1)

(require 'imenu-anywhere)
(require 'ivy)
(ivy-mode t)

(require 'undo-tree)
(global-undo-tree-mode t)

;;
(global-flycheck-mode +1)

;; flyspell-mode does spell-checking on the fly as you type
(require 'flyspell)
(setq ispell-program-name "/usr/local/bin/aspell" ; use aspell instead of ispell
      ispell-extra-args '("--sug-mode=ultra"))


(require 'whitespace)
(whitespace-mode +1)
(setq whitespace-line-column 80) ;; limit line length
(setq whitespace-style '(face tabs empty trailing lines-tail))

(add-hook 'text-mode-hook (lambda () (flyspell-mode +1)))
(add-hook 'text-mode-hook (lambda()
                            (add-hook 'before-save-hook
                                      (lambda () (whitespace-cleanup)) nil t)
                            (whitespace-mode +1)))

;; See here for details https://editorconfig.org/
(require 'editorconfig)
(editorconfig-mode +1)

;; Enable encryption
(require 'epa-file)
(epa-file-enable)

(require 'multiple-cursors)
(global-set-key (kbd "C-c m c") 'mc/edit-lines)

(provide 'my-editor)
;;; my-editor.el ends here

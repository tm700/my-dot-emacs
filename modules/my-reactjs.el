;;;;

;;; Code:

(my-dot-emacs-install-package 'js2-mode)

(use-package js2-mode
:config
(setq js-indent-level 2))

(add-to-list 'auto-mode-alist '("\\.js\\'" . js-jsx-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . js-jsx-mode))

(defun set-jsx-indentation ()
  (web-mode)
  (setq-local sgml-basic-offset js2-basic-offset))

(add-hook 'js2-jsx-mode-hook #'set-jsx-indentation)

(provide 'my-reactjs)
;;;

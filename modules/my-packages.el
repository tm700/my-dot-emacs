;;;;
;;;;

(require 'cl)
(require 'package)

;; accessing a package repo over https on Windows is a no go, so we
;; fallback to http there
;; accessing a package repo over https on Windows is a no go, so we
;; fallback to http there
(if (eq system-type 'windows-nt)
    (add-to-list 'package-archives
                 '("melpa" . "http://melpa.org/packages/") t)
  (add-to-list 'package-archives
               '("melpa" . "https://melpa.org/packages/") t))

(package-initialize)

(defvar my-dot-emacs-packages
  '(
    browse-kill-ring
    diff-hl
    dracula-theme
    editorconfig
    eshell-git-prompt
    flycheck
    flyspell
    imenu-anywhere
    ivy
    magit
    markdown-mode
    multiple-cursors
    smartparens
    super-save
    undo-tree
    use-package
    which-key
    yaml-mode
    beacon)
  "A list of packages to ensure are installed at launch.")

(defun my-dot-emacs-install-package (package)
  (unless (package-installed-p package)
    (package-refresh-contents)
    (package-install package)))

(dolist (pkg my-dot-emacs-packages)
  (my-dot-emacs-install-package pkg))

(provide 'my-packages)

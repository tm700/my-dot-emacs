;;;;


;; load-path

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(defvar my-dot-emacs-root-dir (file-name-directory load-file-name))
(defvar my-dot-emacs-savefile-dir (expand-file-name "savefile" my-dot-emacs-root-dir))

(unless (file-exists-p my-dot-emacs-savefile-dir)
  (make-directory my-dot-emacs-savefile-dir))

(add-to-list 'load-path (concat my-dot-emacs-root-dir "modules"))

(setq custom-file (expand-file-name "my-dot-emacs-custom.el" my-dot-emacs-root-dir))

(load custom-file)

(require 'my-packages)
(require 'my-editor)
(require 'my-global-keybindings)
(require 'my-modes)
(require 'my-programming)
(require 'my-ruby)
(require 'my-kotlin)
(require 'my-web)
(require 'my-macos)
(require 'my-eshell)
(require 'my-utils)
(require 'my-reactjs)
(require 'my-ui)

(server-start)

;;;
(put 'upcase-region 'disabled nil)
